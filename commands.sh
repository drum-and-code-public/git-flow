cd tmp || exit
rm -rf ./.git
git init

# making initial commit to master
git commit --allow-empty -m "Initial Commit"

# here we creating a branch for the first version
git checkout -b v0.0.1

# implementing some functionality
git commit --allow-empty -m "GD-11 Task Description"
git commit --allow-empty -m "GD-12 Task Description"
git commit --allow-empty -m "GD-13 Task Description"

# tagging commit in branch v0.0.1 - this is first production release
# after this we can call branch v0.0.1 - stable
git tag v0.0.1.1

# after any fixes to stable release branch - we need to merge changes to master and other release branch
git checkout master
git merge v0.0.1

# now we creating new branch for new features
git checkout -b v0.0.2

# making some functionality
git commit --allow-empty -m "GD-21 Task Description"
git commit --allow-empty -m "GD-22 Task Description"
git commit --allow-empty -m "GD-23 Task Description"

# client asked for some critical fix on production, so let's checkout to stable branch v0.0.1
git checkout v0.0.1

# making some quick fix
git commit --allow-empty -m "GD-14 HotFix Description"

# tagging commit, making release
git tag v0.0.1.2

# branch is stable, so lets merge changes to master and other release branches
git checkout v0.0.2
git merge v0.0.1

git checkout master
git merge v0.0.1

# return back to work under next version
git checkout v0.0.2
git commit --allow-empty -m "GD-24 Task Description"
git commit --allow-empty -m "GD-25 Task Description"

# client asked for production hotfix again
git checkout v0.0.1
git commit --allow-empty -m "GD-15 HotFix Description"
git tag v0.0.1.3

# again merging stable branch to master and other release branches
git checkout v0.0.2
git merge v0.0.1

git checkout master
git merge v0.0.1

# now we understand that v0.0.2 is very close to release, and we don't want to put new features into v0.0.2
# so we creating v0.0.3
git checkout -b v0.0.3

# making some functionality
git commit --allow-empty -m "GD-31 Task Description"
git commit --allow-empty -m "GD-32 Task Description"
git commit --allow-empty -m "GD-33 Task Description"

# client asked for production hotfix again
git checkout v0.0.1
git commit --allow-empty -m "GD-16 HotFix Description"
git tag v0.0.1.4

# again merging stable branch to master and other release branches
git checkout v0.0.3
git merge v0.0.1

git checkout v0.0.2
git merge v0.0.1

git checkout master
git merge v0.0.1

# we need to do some work under branch v0.0.2
git checkout v0.0.2
git commit --allow-empty -m "GD-26 Task Description"

# all good, v0.0.2 is ready for release - making release
# v0.0.2 became stable
git tag v0.0.2.1

# merging stable branch to master and other release branches
# please note, v0.0.1 is not actual anymore
git checkout v0.0.3
git merge v0.0.2

git checkout master
git merge v0.0.2

# again we have some hotfixes for v0.0.2
git checkout v0.0.2
git commit --allow-empty -m "GD-27 HotFix Description"
git commit --allow-empty -m "GD-28 HotFix Description"
git commit --allow-empty -m "GD-29 HotFix Description"

# today is friday so we unable to release this fixes
# but we should merge v0.0.2 to v0.0.3 and to master
# in order to prevent possible conflicts in future
git checkout v0.0.3
git merge v0.0.2

git checkout master
git merge v0.0.2

# continue working on v0.0.3
git checkout v0.0.3
git commit --allow-empty -m "GD-34 Task Description"
git commit --allow-empty -m "GD-35 Task Description"
git commit --allow-empty -m "GD-36 Task Description"

# and finally we want to make some new feature for release v0.0.4
# but we did not have a branch for this release so we can push this changes to master
# please note only repository maintainer should be able to do this
git checkout master
git commit --allow-empty -m "GD-41 Task Description"
git commit --allow-empty -m "GD-42 Task Description"
git commit --allow-empty -m "GD-43 Task Description"

# please note, the example above is not a guide how to work
# this is just for showing how we should deal with versions
# in real life you should not commit directly to release branch or master
# instead of this you should create Merge Requests to target branch

# if you have ideas for more complex examples
# feel free to ask Bogdan to add this examples
